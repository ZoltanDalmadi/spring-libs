package hu.dolphio.springlibs.auth.jwt;

import hu.dolphio.springlibs.auth.AuthException;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@RequiredArgsConstructor
public class JwtAuthenticationManager implements ReactiveAuthenticationManager {

    private final Function<Claims, Authentication> converter;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        String jwtToken = String.class.cast(authentication.getPrincipal());

        // TODO: this is very basic
        try {
            Jws<Claims> s3cr3t = Jwts.parser()
                .setSigningKey("secret".getBytes())
                .parseClaimsJws(jwtToken);

            return Mono.just(converter.apply(s3cr3t.getBody()));
        } catch (SignatureException | ExpiredJwtException e) {
            // don't trust the JWT!
            return Mono.error(AuthException.of(HttpStatus.UNAUTHORIZED, e.getMessage()));
        }
    }
}
