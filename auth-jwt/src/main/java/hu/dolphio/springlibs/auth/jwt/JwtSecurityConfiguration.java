package hu.dolphio.springlibs.auth.jwt;

import hu.dolphio.springlibs.auth.AbstractSecurityConfiguration;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.ReactiveAuthenticationManager;

public abstract class JwtSecurityConfiguration extends AbstractSecurityConfiguration<Claims> {

    @Override
    public ReactiveAuthenticationManager authenticationManager() {
        return new JwtAuthenticationManager(successfulAuthenticationConverter());
    }

}
