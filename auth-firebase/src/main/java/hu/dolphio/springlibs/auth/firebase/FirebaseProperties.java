package hu.dolphio.springlibs.auth.firebase;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("spring.firebase")
class FirebaseProperties {
    private String databaseUrl;
    private String configPath;
}
