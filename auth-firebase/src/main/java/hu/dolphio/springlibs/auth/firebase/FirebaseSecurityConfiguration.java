package hu.dolphio.springlibs.auth.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseToken;
import hu.dolphio.springlibs.auth.AbstractSecurityConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.authentication.ReactiveAuthenticationManager;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;

@Slf4j
@EnableConfigurationProperties(FirebaseProperties.class)
public abstract class FirebaseSecurityConfiguration extends AbstractSecurityConfiguration<FirebaseToken> {

    @Autowired
    private FirebaseProperties properties;

    @PostConstruct
    public void init() throws IOException {
        FileInputStream serviceAccount = new FileInputStream(properties.getConfigPath());
        FirebaseOptions options = new FirebaseOptions.Builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .setDatabaseUrl(properties.getDatabaseUrl())
            .build();

        FirebaseApp.initializeApp(options);
        log.info("Successfully initialized Firebase app");
    }

    @Override
    public ReactiveAuthenticationManager authenticationManager() {
        return new FirebaseAuthenticationManager(successfulAuthenticationConverter());
    }

}
