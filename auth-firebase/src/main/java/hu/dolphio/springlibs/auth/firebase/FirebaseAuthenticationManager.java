package hu.dolphio.springlibs.auth.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import hu.dolphio.springlibs.auth.AuthException;
import hu.dolphio.springlibs.utils.ReactorUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@RequiredArgsConstructor
public class FirebaseAuthenticationManager implements ReactiveAuthenticationManager {

    private final Function<FirebaseToken, Authentication> converter;
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        String token = (String) authentication.getPrincipal();

        return ReactorUtils.wrapToMono(() -> firebaseAuth.verifyIdTokenAsync(token).get())
            .onErrorMap(e -> AuthException.of(HttpStatus.UNAUTHORIZED, e.getMessage()))
            .map(converter);
    }

}
