package hu.dolphio.springlibs.utils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.function.Function;

public abstract class ReactorUtils {

    /**
     * Wraps a synchronous method call into a {@link Mono}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called when the returned {@link Mono} is subscribed,
     * and its return value will be wrapped in it.
     *
     * @param callable The method that will be called
     * @param <T>      The type of the method's return value
     * @return         The wrapped value in a {@link Mono}
     */
    public static <T> Mono<T> wrapToMono(Callable<T> callable) {
        return Mono.fromCallable(callable).subscribeOn(Schedulers.elastic());
    }

    /**
     * Wraps a synchronous method call into a {@link Flux}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called when the returned {@link Flux} is subscribed,
     * and its return value (which is an {@link Iterable}) will be wrapped in it.
     *
     * @param callable The method that will be called
     * @param <T>      The returned {@link Iterable}'s type
     * @return         The {@link Flux} that is created from the {@link Iterable}
     */
    public static <T> Flux<T> wrapToFlux(Callable<? extends Iterable<T>> callable) {
        return wrapToMono(callable).flatMapMany(Flux::fromIterable);
    }

    /**
     * Wraps a synchronous method call into a {@link Mono}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called with the provided argument when the returned {@link Mono} is subscribed,
     * and its return value will be wrapped in it.
     *
     * @param function The method that will be called
     * @param arg      The argument to the method
     * @param <T>      The return type of the method
     * @param <A>      The argument type
     * @return         The method's return value wrapped in a {@link Mono}
     */
    public static <T, A> Mono<T> wrapToMono(Function<A, T> function, A arg) {
        return wrapToMono(() -> function.apply(arg));
    }

    /**
     * Wraps a synchronous method call into a {@link Flux}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called with the provided argument when the returned {@link Flux} is subscribed,
     * and its return value will be wrapped in it.
     *
     * @param function The method that will be called
     * @param arg      The argument to the method
     * @param <T>      The type of the {@link Iterable} returned by the method
     * @param <A>      The argument type
     * @return         The method's return value wrapped in a {@link Flux}
     */
    public static <T, A> Flux<T> wrapToFlux(Function<A, ? extends Iterable<T>> function, A arg) {
        return wrapToFlux(() -> function.apply(arg));
    }

    /**
     * Wraps a synchronous method call into a {@link Mono}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called with the provided arguments when the returned {@link Mono} is subscribed,
     * and its return value will be wrapped in it.
     *
     * @param function The method that will be called
     * @param arg1     The first argument to the method
     * @param arg2     The second argument to the method
     * @param <T>      The return type of the method
     * @param <A1>     The first argument's type
     * @param <A2>     The second argument's type
     * @return         The method's return value wrapped in a {@link Mono}
     */
    public static <T, A1, A2> Mono<T> wrapToMono(BiFunction<A1, A2, T> function, A1 arg1, A2 arg2) {
        return wrapToMono(() -> function.apply(arg1, arg2));
    }

    /**
     * Wraps a synchronous method call into a {@link Flux}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called with the provided arguments when the returned {@link Flux} is subscribed,
     * and its return value will be wrapped in it.
     *
     * @param function The method that will be called
     * @param arg1     The first argument to the method
     * @param arg2     The second argument to the method
     * @param <T>      The return type of the method
     * @param <A1>     The first argument's type
     * @param <A2>     The second argument's type
     * @return         The method's return value wrapped in a {@link Flux}
     */
    public static <T, A1, A2> Flux<T> wrapToFlux(BiFunction<A1, A2, ? extends Iterable<T>> function, A1 arg1, A2 arg2) {
        return wrapToFlux(() -> function.apply(arg1, arg2));
    }

    /**
     * Wraps a synchronous method call into a {@link Mono}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called with the provided arguments when the returned {@link Mono} is subscribed,
     * and its return value will be wrapped in it.
     *
     * @param function The method that will be called
     * @param arg1     The first argument to the method
     * @param arg2     The second argument to the method
     * @param arg3     The third argument to the method
     * @param <T>      The return type of the method
     * @param <A1>     The first argument's type
     * @param <A2>     The second argument's type
     * @param <A3>     The third argument's type
     * @return         The method's return value wrapped in a {@link Mono}
     */
    public static <T, A1, A2, A3> Mono<T> wrapToMono(TriFunction<A1, A2, A3, T> function, A1 arg1, A2 arg2, A3 arg3) {
        return wrapToMono(() -> function.apply(arg1, arg2, arg3));
    }

    /**
     * Wraps a synchronous method call into a {@link Flux}.
     * This can be used for adapting non-asynchronous / non-reactive APIs into the reactive world.
     *
     * The provided method will be called with the provided arguments when the returned {@link Flux} is subscribed,
     * and its return value will be wrapped in it.
     *
     * @param function The method that will be called
     * @param arg1     The first argument to the method
     * @param arg2     The second argument to the method
     * @param arg3     The third argument to the method
     * @param <T>      The return type of the method
     * @param <A1>     The first argument's type
     * @param <A2>     The second argument's type
     * @param <A3>     The third argument's type
     * @return         The method's return value wrapped in a {@link Flux}
     */
    public static <T, A1, A2, A3> Flux<T> wrapToFlux(TriFunction<A1, A2, A3, ? extends Iterable<T>> function, A1 arg1, A2 arg2, A3 arg3) {
        return wrapToFlux(() -> function.apply(arg1, arg2, arg3));
    }

}
