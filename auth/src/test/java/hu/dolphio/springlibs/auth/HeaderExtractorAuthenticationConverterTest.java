package hu.dolphio.springlibs.auth;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class HeaderExtractorAuthenticationConverterTest {

    private HeaderExtractorAuthenticationConverter converter;

    @BeforeEach
    void setUp() {
        converter = new HeaderExtractorAuthenticationConverter();
    }

    @Test
    @DisplayName("Should return Mono<TokenContainer> with token")
    void allOk() {
        MockServerHttpRequest request = MockServerHttpRequest.get("/test")
            .header(HttpHeaders.AUTHORIZATION, "Bearer TEST-TOKEN")
            .build();

        MockServerWebExchange exchange = MockServerWebExchange.from(request);

        Mono<Authentication> result = converter.apply(exchange);

        StepVerifier.create(result)
            .expectNext(new TokenContainer("TEST-TOKEN"))
            .verifyComplete();
    }

    @Test
    @DisplayName("Should return empty Mono when no Authorization header found")
    void noAuthorizationHeader() {
        MockServerHttpRequest request = MockServerHttpRequest.get("/test")
            .build();

        MockServerWebExchange exchange = MockServerWebExchange.from(request);

        Mono<Authentication> result = converter.apply(exchange);

        StepVerifier.create(result)
            .verifyComplete();
    }

    @Test
    @DisplayName("Should return empty Mono when Authorization header does not start with prefix")
    void notStartsWithPrefix() {
        MockServerHttpRequest request = MockServerHttpRequest.get("/test")
            .header(HttpHeaders.AUTHORIZATION, "NotPrefix TEST-TOKEN")
            .build();

        MockServerWebExchange exchange = MockServerWebExchange.from(request);

        Mono<Authentication> result = converter.apply(exchange);

        StepVerifier.create(result)
            .verifyComplete();
    }
}