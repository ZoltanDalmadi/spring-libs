package hu.dolphio.springlibs.auth;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AuthException extends ResponseStatusException {

    public static AuthException of(HttpStatus status, String reason) {
        return new AuthException(status, reason);
    }

    private AuthException(HttpStatus status, String reason) {
        super(status, reason);
    }

    @Override
    public String getMessage() {
        return getReason();
    }
}
