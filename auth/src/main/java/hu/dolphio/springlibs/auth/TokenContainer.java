package hu.dolphio.springlibs.auth;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;

public class TokenContainer extends AbstractAuthenticationToken {
    @Getter
    @Setter
    private String token;

    public TokenContainer() {
        super(null);
    }

    public TokenContainer(String token) {
        this();
        this.token = token;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }
}
