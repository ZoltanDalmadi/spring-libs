package hu.dolphio.springlibs.auth;

import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public abstract class AbstractSecurityConfiguration<T> {

    public List<String> whitelist() {
        return Collections.emptyList();
    }

    protected Function<ServerWebExchange, Mono<Authentication>> preAuthenticationConverter() {
        return new HeaderExtractorAuthenticationConverter();
    }

    @Bean
    public abstract ReactiveAuthenticationManager authenticationManager();

    protected abstract Function<T, Authentication> successfulAuthenticationConverter();

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        val authenticationWebFilter = new AuthenticationWebFilter(authenticationManager());
        authenticationWebFilter.setAuthenticationConverter(preAuthenticationConverter());

        http.headers().frameOptions().disable();
        http.csrf().disable();

        val authorizeExchangeSpec = http.authorizeExchange();
        val whiteList = whitelist();

        if (!whiteList.isEmpty()) {
            authorizeExchangeSpec.pathMatchers(whiteList.toArray(new String[0])).permitAll();
        }

        authorizeExchangeSpec.anyExchange().authenticated();

        http.addFilterAt(authenticationWebFilter, SecurityWebFiltersOrder.AUTHENTICATION);

        return http.build();
    }

}
