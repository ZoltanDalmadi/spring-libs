package hu.dolphio.springlibs.auth;

import lombok.Setter;
import lombok.val;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@Setter
public class HeaderExtractorAuthenticationConverter implements Function<ServerWebExchange, Mono<Authentication>> {

    private String headerKey = HttpHeaders.AUTHORIZATION;
    private String prefix = "Bearer ";

    @Override
    public Mono<Authentication> apply(ServerWebExchange exchange) {
        val headers = exchange.getRequest().getHeaders();
        val authorization = headers.getFirst(headerKey);

        return StringUtils.hasText(authorization) && authorization.startsWith(prefix)
            ? Mono.just(new TokenContainer(authorization.substring(prefix.length())))
            : Mono.empty();
    }

}
